<?php

namespace App\Tests;

use App\Entity\Sale;
use App\Repository\SaleRepository;
use ErrorException;
use PHPUnit\Framework\TestCase;
use App\Business\BookBusiness;
use App\Repository\BookRepository;
use App\Repository\OrderRepository;
use App\Entity\Book;

class BookBusinessTest extends TestCase
{
    private BookBusiness $bookBusiness;
    private BookRepository $bookRepository;
    private SaleRepository $saleRepository;
    private OrderRepository $orderRepository;

    protected function setUp(): void
    {
        $this->bookRepository = $this->createMock(BookRepository::class);
        $this->saleRepository = $this->createMock(SaleRepository::class);
        $this->orderRepository = $this->createMock(OrderRepository::class);

        $this->bookBusiness = new BookBusiness($this->bookRepository, $this->saleRepository, $this->orderRepository);
    }

    public function testSellBookSuccess()
    {
        $idBook = 1;
        $quantity = 2;
        $idUser = 1;
        $saleDate = '2023-10-01';
        $book = new Book();
        $book->setId($idBook);
        $book->setStock(5);
        $this->bookRepository->method('getByIdSQL')->willReturn($book);
        $this->saleRepository->expects($this->once())->method('addSQL')->with($this->isInstanceOf(Sale::class));
        $this->bookRepository->expects($this->once())->method('updateSQL')->with($this->callback(function ($updatedBook){
            return $updatedBook->getStock() === 3;
        }));
        $sale = $this->bookBusiness->sellBook($idBook, $quantity, $idUser, $saleDate);
        $this->assertInstanceOf(Sale::class, $sale);
        $this->assertEquals($quantity, $sale->getBookQuantity());
        $this->assertEquals($idUser, $sale->getIdUser());
        $this->assertEquals($idBook, $sale->getIdBook());
    }

    public function testSellBookNotEnoughStock()
    {
        $this->expectException(ErrorException::class);
        $this->expectExceptionMessage("Not enough stock");
        $idBook = 1;
        $quantity = 10; 
        $idUser = 1;
        $saleDate = '2023-10-01';
        $book = new Book();
        $book->setId($idBook);
        $book->setStock(5); 
        $this->bookRepository->method('getByIdSQL')->willReturn($book);
        $this->bookBusiness->sellBook($idBook, $quantity, $idUser, $saleDate);
    }
}

