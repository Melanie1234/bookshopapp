-- Active: 1673947741321@@127.0.0.1@3306@bookshop
-- Afficher le livre le plus vendu
SELECT title AS book_info, SUM(book_quantity) AS sale_max FROM book INNER JOIN sale ON book.id = sale.id_book GROUP BY book.id ORDER BY sale_max DESC;