<?php

namespace App\Business;

use App\Entity\Order;
use App\Entity\Sale;
use App\Repository\BookRepository;
use App\Repository\OrderRepository;
use App\Repository\SaleRepository;
use ErrorException;

class BookBusiness
{
    public function __construct(private BookRepository $bookRepository, private SaleRepository $saleRepository, private OrderRepository $orderRepository)
    {
    }

    public function sellBook(int $idBook, int $quantity, int $idUser, string $saleDate)
    {
        $sale = new Sale();
        $book = $this->bookRepository->getByIdSQL($idBook);
        $sale->setSaleDate(new \DateTime($saleDate));
        $sale->setBookQuantity($quantity);
        $sale->setIdUser($idUser);
        $sale->setIdBook($idBook);
        if ($sale->getBookQuantity() <= $book->getStock()) {
            $this->saleRepository->addSQL($sale);
            $book->setStock($book->getStock() - $sale->getBookQuantity());
            $this->bookRepository->updateSQL($book);
            return $sale;
        }
        throw new ErrorException("Not enough stock");
    }

    public function validateOrder(int $idBook, int $quantity, int $idUser)
    {
        $order = new Order();
        $book = $this->bookRepository->getByIdSQL($idBook);
        $order->setReceptionDate(new \DateTime());
        $order->setBookQuantity($quantity);
        $order->setTotal($quantity * $book->getPrice());
        $order->setIdUser($idUser);
        $order->setIdBook($idBook);
        $this->orderRepository->addSQL($order);
        $book->setStock($book->getStock() + $order->getBookQuantity());
        $this->bookRepository->updateSQL($book);
        return $order;
    }
}