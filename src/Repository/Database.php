<?php 

namespace App\Repository;

class Database {
    public static function getConnection() {
        return new \PDO("mysql:host={$_ENV['DB_HOST']};dbname={$_ENV['DB_NAME']}", $_ENV['DB_USER'], $_ENV['DB_PASSWORD']);
    }
}