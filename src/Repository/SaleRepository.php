<?php

namespace App\Repository;

use App\Entity\Book;
use App\Entity\Sale;
use App\Entity\User;
use PDO;

class SaleRepository extends AbstractRepository
{
    private PDO $connection;

    public function __construct()
    {
        parent::__construct(
            'INSERT INTO sale (sale_date,book_quantity,id_user,id_book) VALUES (:sale_date,:book_quantity,:id_user,:id_book)',
            'SELECT * FROM sale',
            'SELECT * FROM sale WHERE id=:id',
            'UPDATE sale SET sale_date=:sale_date,book_quantity=:book_quantity,id_user=:id_user,id_book=:id_book WHERE id=:id',
            'DELETE FROM sale WHERE id=:id'
        );
    }

    protected function sqlToEntity($rs)
    {
        return new Sale(
            new \DateTime($rs["sale_date"]),
            $rs["book_quantity"],
            $rs["id_user"],
            $rs["id_book"],
            $rs["id"],
        );
    }

    protected function entityBindValues($stmt, $entity)
    {
        $stmt->bindValue("sale_date", $entity->getSaleDate()->format("Y-m-d"));
        $stmt->bindValue("book_quantity", $entity->getBookQuantity());
        $stmt->bindValue("id_user", $entity->getIdUser());
        $stmt->bindValue("id_book", $entity->getIdBook());
    }

    protected function entityBindValuesWithId($stmt, $entity)
    {
        $this->entityBindValues($stmt, $entity);
        $stmt->bindValue('id', $entity->getId());
    }

    public function allSalesInfos()
    {
        $connection = Database::getConnection();
        $stmt = $connection->prepare('SELECT s.id as saleid, s.*, b.*, u.*
        FROM sale as s
            JOIN book as b ON s.id_book = b.id
            JOIN user as u ON s.id_user = u.id
            ORDER BY sale_date');
        $stmt->execute();

        $sales = [];
        $results = $stmt->fetchAll();
        foreach ($results as $result) {
            $sale = new Sale(
                new \DateTime($result["sale_date"]),
                $result["book_quantity"],
                $result["id_user"],
                $result["id_book"],
                $result["saleid"],
            );
            $book = new Book(
                $result["title"],
                $result["isbn_13"],
                new \DateTime($result["publication_date"]),
                $result["comment"],
                $result["stock"],
                $result["status"],
                $result["price"],
                $result["id_author"],
                $result["id_genre"],
                $result["id_publisher"],
                $result["id_book"],
            );
            $sale->setBook($book);
            $user = new User(
                $result["firstname"],
                $result["lastname"],
                $result["email"],
                $result["bookshop"],
                $result["password"],
                $result["id_admin"],
                $result["role"],
                $result["id_user"]
            );
            $sale->setUser($user);
            array_push($sales, $sale);
        }
        return $sales;
    }

    public function oneSaleInfos($id)
    {
        $connection = Database::getConnection();
        $stmt = $connection->prepare('SELECT s.id as saleid, s.*, b.*, u.*
        FROM sale as s
            JOIN book as b ON s.id_book = b.id
            JOIN user as u ON s.id_user = u.id
        WHERE s.id=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();

        $result = $stmt->fetch();
        $sale = new Sale(
            new \DateTime($result["sale_date"]),
            $result["book_quantity"],
            $result["id_user"],
            $result["id_book"],
            $result["id"],
        );
        $book = new Book(
            $result["title"],
            $result["isbn_13"],
            new \DateTime($result["publication_date"]),
            $result["comment"],
            $result["stock"],
            $result["status"],
            $result["price"],
            $result["id_author"],
            $result["id_genre"],
            $result["id_publisher"],
            $result["id_book"],
        );
        $sale->setBook($book);
        $user = new User(
            $result["firstname"],
            $result["lastname"],
            $result["email"],
            $result["bookshop"],
            $result["password"],
            $result["id_admin"],
            $result["role"],
            $result["id_user"]
        );
        $sale->setUser($user);
        return $sale;
    }

    public function postSaleInfos(Sale $sale)
    {
        $connection = Database::getConnection();
        $stmt = $connection->prepare('INSERT INTO sale (sale_date,book_quantity,id_user,id_book) VALUES (:sale_date,:book_quantity,:id_user,:id_book) VALUES (:sale_date,:book_quantity,:id_user,:id_book)');

        $saleDate = $sale->getSaleDate()->format('Y-m-d');
        $bookQuantity = $sale->getBookQuantity();
        $idUser = $sale->getIdUser();
        $idBook = $sale->getIdBook();

        $stmt->bindParam(':sale_date', $saleDate);
        $stmt->bindParam(':book_quantity', $bookQuantity);
        $stmt->bindParam(':id_user', $idUser);
        $stmt->bindParam(':id_book', $idBook);

        $stmt->execute();

        $sale->setId($connection->lastInsertId());
        return $sale;
    }
}