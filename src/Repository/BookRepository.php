<?php

namespace App\Repository;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Genre;
use App\Entity\Publisher;
use PDO;

class BookRepository extends AbstractRepository
{
    private PDO $connection;
    private AuthorRepository $authorRepository;

    public function __construct()
    {
        parent::__construct('INSERT INTO book (title,isbn_13,publication_date,`comment`,stock,`status`,price,id_genre,id_author,id_publisher) 
        VALUES (:title, :isbn_13, :publication_date, :comment, :stock, :status, :price, :id_genre, :id_author, :id_publisher)',
            'SELECT * FROM book',
            'SELECT * FROM book WHERE id=:id',
            'UPDATE book SET title=:title,isbn_13=:isbn_13,publication_date=:publication_date,`comment`=:comment,stock=:stock,`status`=:status,price=:price,id_genre=:id_genre,id_author=:id_author,id_publisher=:id_publisher WHERE id=:id',
            'DELETE FROM book WHERE id=:id'
        );
    }

    protected function sqlToEntity($rs)
    {
        return new Book(
            $rs["title"],
            $rs["isbn_13"],
            new \DateTime($rs["publication_date"]),
            $rs["comment"],
            $rs["stock"],
            $rs["status"],
            $rs["price"],
            $rs["id_genre"],
            $rs["id_author"],
            $rs["id_publisher"],
            $rs["id"],
        );
    }
    protected function entityBindValues($stmt, $entity)
    {
        $stmt->bindValue("title", $entity->getTitle());
        $stmt->bindValue("isbn_13", $entity->getIsbn13());
        $stmt->bindValue("publication_date", $entity->getPublicationDate()->format("Y-m-d"));
        $stmt->bindValue("comment", $entity->getComment());
        $stmt->bindValue("stock", $entity->getStock());
        $stmt->bindValue("status", $entity->getStatus());
        $stmt->bindValue("price", $entity->getPrice());
        $stmt->bindValue("id_genre", $entity->getIdGenre());
        $stmt->bindValue("id_author", $entity->getIdAuthor());
        $stmt->bindValue("id_publisher", $entity->getIdPublisher());
    }

    protected function entityBindValuesWithId($stmt, $entity)
    {
        $this->entityBindValues($stmt, $entity);
        $stmt->bindValue('id', $entity->getId());
    }

    public function mostSoldBook()
    {
        $connection = Database::getConnection();
        $stmt = $connection->prepare('SELECT title AS book_info, SUM(book_quantity) AS sale_max 
        FROM book 
        INNER JOIN sale ON book.id = sale.id_book 
        GROUP BY book.id 
        ORDER BY sale_max DESC');
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function allBooksInfos()
    {
        $connection = Database::getConnection();
        $stmt = $connection->prepare('SELECT b.id as bookid, b.*, a.*, p.*, g.*
        FROM book as b
            JOIN author as a ON b.id_author = a.id
            JOIN publisher as p ON b.id_publisher = p.id
            JOIN genre as g ON b.id_genre = g.id
            ORDER BY bookid');
        $stmt->execute();

        $books = [];
        $results = $stmt->fetchAll();
        foreach ($results as $result) {
            $book = new Book(
                $result["title"],
                $result["isbn_13"],
                new \DateTime($result["publication_date"]),
                $result["comment"],
                $result["stock"],
                $result["status"],
                $result["price"],
                $result["id_author"],
                $result["id_genre"],
                $result["id_publisher"],
                $result["bookid"],
            );
            $author = new Author(
                $result["name"],
                $result["lastname"],
                new \DateTime($result["birthdate"]),
                $result["id_author"]
            );
            $book->setAuthor($author);
            $genre = new Genre(
                $result["genre_label"],
                $result["id_genre"]
            );
            $book->setGenre($genre);
            $publisher = new Publisher(
                $result["publisher_label"],
                $result["id_publisher"]
            );
            $book->setPublisher($publisher);
            array_push($books, $book);
        }
        return $books;
    }

    public function oneBookInfos($id)
    {
        $connection = Database::getConnection();
        $stmt = $connection->prepare('SELECT b.id as bookid, b.*, a.*, p.*, g.*
        FROM book as b
            JOIN author as a ON b.id_author = a.id
            JOIN publisher as p ON b.id_publisher = p.id
            JOIN genre as g ON b.id_genre = g.id
        WHERE b.id=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();

        $result = $stmt->fetch();
        $book = new Book(
            $result["title"],
            $result["isbn_13"],
            new \DateTime($result["publication_date"]),
            $result["comment"],
            $result["stock"],
            $result["status"],
            $result["price"],
            $result["id_genre"],
            $result["id_author"],
            $result["id_publisher"],
            $result["bookid"],
        );
        $author = new Author(
            $result["name"],
            $result["lastname"],
            new \DateTime($result["birthdate"]),
            $result["id"]
        );
        $book->setAuthor($author);
        $genre = new Genre(
            $result["genre_label"],
            $result["id"]
        );
        $book->setGenre($genre);
        $publisher = new Publisher(
            $result["publisher_label"],
            $result["id"]
        );
        $book->setPublisher($publisher);
        return $book;
    }

    public function postBookInfos(Book $book)
    {
        $connection = Database::getConnection();
        $stmt = $connection->prepare('INSERT INTO book (title,isbn_13,publication_date,`comment`,stock,`status`,price,id_genre,id_author,id_publisher) VALUES (:title, :isbn_13, :publication_date, :comment, :stock, :status, :price, :id_genre, :id_author, :id_publisher)');

        $title = $book->getTitle();
        $isbn13 = $book->getIsbn13();
        $publicationDate = $book->getPublicationDate()->format('Y-m-d');
        $comment = $book->getComment();
        $stock = $book->getStock();
        $status = $book->getStatus();
        $price = $book->getPrice();
        $idGenre = $book->getIdGenre();
        $idAuthor = $book->getIdAuthor();
        $idPublisher = $book->getIdPublisher();

        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':isbn_13', $isbn13);
        $stmt->bindParam(':publication_date', $publicationDate);
        $stmt->bindParam(':comment', $comment);
        $stmt->bindParam(':stock', $stock);
        $stmt->bindParam(':status', $status);
        $stmt->bindParam(':price', $price);
        $stmt->bindParam(':id_genre', $idGenre);
        $stmt->bindParam(':id_author', $idAuthor);
        $stmt->bindParam(':id_publisher', $idPublisher);

        $stmt->execute();

        $book->setId($connection->lastInsertId());
        return $book;
    }

    public function findOneByIsbn(string $isbn13): ?Book
    {
        $connection = Database::getConnection();
        $stmt = $connection->prepare('SELECT * FROM book WHERE isbn_13 = :isbn');
        $stmt->bindValue(':isbn', $isbn13);
        $stmt->execute();
        $result = $stmt->fetch();
        if (!$result) {
            return null; 
        }
        return $this->sqlToEntity($result);
    }
}