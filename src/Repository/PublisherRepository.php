<?php

namespace App\Repository;

use App\Entity\Publisher;
use PDO;

class PublisherRepository extends AbstractRepository
{
    private PDO $connection;

    public function __construct()
    {
        parent::__construct('INSERT INTO publisher (publisher_label) VALUES
         (:publisher_label)', 'SELECT * FROM publisher', 'SELECT * FROM publisher WHERE id=:id', 'UPDATE publisher SET publisher_label=:publisher_label WHERE id=:id', 'DELETE FROM publisher WHERE id=:id');
    }

    protected function sqlToEntity($rs)
    {
        return new Publisher(
            $rs["publisher_label"],
            $rs["id"],
        );
    }

    protected function entityBindValues($stmt, $entity)
    {
        // $stmt->bindValue("id", $entity->getId());
        $stmt->bindValue("publisher_label", $entity->getPublisherLabel());
    }

    protected function entityBindValuesWithId($stmt, $entity)
    {
        $this->entityBindValues($stmt, $entity);
        $stmt->bindValue("id", $entity->getId());
    }
}