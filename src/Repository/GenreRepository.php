<?php

namespace App\Repository;

use App\Entity\Genre;
use PDO;

class GenreRepository extends AbstractRepository
{
    private PDO $connection;

    public function __construct()
    {
        parent::__construct('INSERT INTO genre (genre_label) VALUES (:genre_label)', 'SELECT * FROM genre', 'SELECT * FROM genre WHERE id=:id', 'UPDATE genre SET genre_label=:genre_label, WHERE id=:id', 'DELETE FROM genre WHERE id=:id');
    }

    protected function sqlToEntity($rs)
    {
        return new Genre(
            $rs["genre_label"],
            $rs["id"],
        );
    }

    protected function entityBindValues($stmt, $entity)
    {
        $stmt->bindValue("genre_label", $entity->getGenreLabel());
    }

    protected function entityBindValuesWithId($stmt, $entity)
    {
        $this->entityBindValues($stmt, $entity);
        $stmt->bindValue("id", $entity->getId());
    }
}