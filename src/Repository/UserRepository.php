<?php

namespace App\Repository;

use App\Entity\User;
use PDO;

class UserRepository extends AbstractRepository
{
    private PDO $connection;

    public function __construct()
    {
        parent::__construct('INSERT INTO user (firstname, lastname, email, bookshop, `password`, id_admin, `role`) 
        VALUES (:firstname, :lastname, :email, :bookshop, :password, :id_admin, :role)'
            ,
            'SELECT * FROM user',
            'SELECT * FROM user WHERE id=:id',
            'UPDATE user SET firstname=:firstname, lastname=:lastname, email=:email, bookshop=:bookshop, `password`=:password, id_admin=:id_admin, `role`=:role
        WHERE id=:id',
            'DELETE FROM genre WHERE id=:id'
        );
    }

    protected function sqlToEntity($rs)
    {
        return new User(
            $rs["firstname"],
            $rs["lastname"],
            $rs["email"],
            $rs["bookshop"],
            $rs["password"],
            $rs["id_admin"],
            $rs["role"],
            $rs["id"]
        );
    }

    protected function entityBindValues($stmt, $entity)
    {
        $stmt->bindValue("firstname", $entity->getFirstname());
        $stmt->bindValue("lastname", $entity->getLastname());
        $stmt->bindValue("email", $entity->getEmail());
        $stmt->bindValue("bookshop", $entity->getBookshop());
        $stmt->bindValue("password", $entity->getPassword());
        $stmt->bindValue("id_admin", $entity->getIdAdmin());
        $stmt->bindValue("role", $entity->getRole());

    }

    protected function entityBindValuesWithId($stmt, $entity)
    {
        $this->entityBindValues($stmt, $entity);
        $stmt->bindValue("id", $entity->getId());
    }

    public function findByEmail(string $email): ?User
    {
        $connection = Database::getConnection();
        $query = $connection->prepare('SELECT * FROM user WHERE email=:email');
        $query->bindValue(':email', $email);
        $query->execute();
        foreach ($query->fetchAll() as $line) {
            return $this->sqlToEntity($line);
        }
        return null;
    }

}