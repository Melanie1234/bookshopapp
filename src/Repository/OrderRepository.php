<?php

namespace App\Repository;

use App\Entity\Book;
use App\Entity\Order;
use App\Entity\User;
use PDO;

class OrderRepository extends AbstractRepository
{
    private PDO $connection;

    public function __construct()
    {
        parent::__construct(
            'INSERT INTO `order` (reception_date,book_quantity,total,`comment`,id_user,id_book) VALUES (:reception_date,:book_quantity,:total,:comment,:id_user,:id_book)',
            'SELECT * FROM `order`',
            'SELECT * FROM `order` WHERE id=:id',
            'UPDATE `order` SET reception_date=:reception_date,book_quantity=:book_quantity, total=:total,`comment`=:`comment`,id_user=:id_user,id_book=:id_book WHERE id=:id',
            'DELETE FROM `order` WHERE id=:id'
        );
    }

    protected function sqlToEntity($rs)
    {
        return new Order(
            new \DateTime($rs["reception_date"]),
            $rs["book_quantity"],
            $rs["total"],
            $rs["comment"],
            $rs["id_user"],
            $rs["id_book"],
            $rs["id"],
        );
    }

    protected function entityBindValues($stmt, $entity)
    {
        $stmt->bindValue("reception_date", $entity->getReceptionDate()->format('Y-m-d'));
        $stmt->bindValue("book_quantity", $entity->getBookQuantity());
        $stmt->bindValue("total", $entity->getTotal());
        $stmt->bindValue("comment", $entity->getComment());
        $stmt->bindValue("id_user", $entity->getIdUser());
        $stmt->bindValue("id_book", $entity->getIdBook());
    }

    protected function entityBindValuesWithId($stmt, $entity)
    {
        $this->entityBindValues($stmt, $entity);
        $stmt->bindValue('id', $entity->getId());
    }

    public function allOrdersInfos()
    {
        $connection = Database::getConnection();
        $stmt = $connection->prepare('SELECT o.id as orderid, o.*, b.*, u.*
                FROM `order` as o
                    JOIN book as b ON o.id_book = b.id
                    JOIN user as u ON o.id_user = u.id
                    ORDER BY orderid');
        $stmt->execute();

        $orders = [];
        $results = $stmt->fetchAll();
        foreach ($results as $result) {
            $order = new Order(
                new \DateTime($result["reception_date"]),
                $result["book_quantity"],
                $result["total"],
                $result["comment"],
                $result["id_user"],
                $result["id_book"],
                $result["orderid"],
            );
            $book = new Book(
                $result["title"],
                $result["isbn_13"],
                new \DateTime($result["publication_date"]),
                $result["comment"],
                $result["stock"],
                $result["status"],
                $result["price"],
                $result["id_author"],
                $result["id_genre"],
                $result["id_publisher"],
                $result["id_book"],
            );
            $order->setBook($book);
            $user = new User(
                $result["firstname"],
                $result["lastname"],
                $result["email"],
                $result["bookshop"],
                $result["password"],
                $result["id_admin"],
                $result["role"],
                $result["id_user"]
            );
            $order->setUser($user);
            array_push($orders, $order);
        }
        return $orders;
    }

    public function oneOrderInfos($id)
    {
        $connection = Database::getConnection();
        $stmt = $connection->prepare('SELECT o.id as orderid, o.*, b.*, u.*
        FROM order as o
            JOIN book as b ON o.id_book = b.id
            JOIN user as u ON o.id_user = u.id
        WHERE s.id=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();

        $result = $stmt->fetch();
        $sale = new Order(
            new \DateTime($result["reception_date"]),
            $result["book_quantity"],
            $result["total"],
            $result["comment"],
            $result["id_user"],
            $result["id_book"],
            $result["orderid"],
        );
        $book = new Book(
            $result["title"],
            $result["isbn_13"],
            new \DateTime($result["publication_date"]),
            $result["comment"],
            $result["stock"],
            $result["status"],
            $result["price"],
            $result["id_author"],
            $result["id_genre"],
            $result["id_publisher"],
            $result["id_book"],
        );
        $sale->setBook($book);
        $user = new User(
            $result["firstname"],
            $result["lastname"],
            $result["email"],
            $result["bookshop"],
            $result["password"],
            $result["id_admin"],
            $result["role"],
            $result["id_user"]
        );
        $sale->setUser($user);
        return $sale;
    }

    public function postOrderInfos(Order $order)
    {
        $connection = Database::getConnection();
        $stmt = $connection->prepare('INSERT INTO order (reception_date,book_quantity,id_user,id_book) VALUES (:reception_date,:book_quantity,:id_user,:id_book) VALUES (:reception_date,:book_quantity,:id_user,:id_book)');

        $receptionDate = $order->getReceptionDate()->format('Y-m-d');
        $bookQuantity = $order->getBookQuantity();
        $idUser = $order->getIdUser();
        $idBook = $order->getIdBook();

        $stmt->bindParam(':sale_date', $receptionDate);
        $stmt->bindParam(':book_quantity', $bookQuantity);
        $stmt->bindParam(':id_user', $idUser);
        $stmt->bindParam(':id_book', $idBook);

        $stmt->execute();

        $order->setId($connection->lastInsertId());
        return $order;
    }
}