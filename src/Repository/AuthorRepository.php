<?php

namespace App\Repository;

use App\Entity\Author;
use PDO;

class AuthorRepository extends AbstractRepository
{
    private PDO $connection;

    public function __construct()
    {
        parent::__construct('INSERT INTO author (`name`,lastname,birthdate) VALUES (:name,:lastname,:birthdate)',
         'SELECT * FROM author', 'SELECT * FROM author WHERE id=:id', 'UPDATE author SET `name`=:name,lastname=:lastname,birthdate=:birthdate
          WHERE id=:id', 'DELETE FROM author WHERE id=:id');
    }
    protected function sqlToEntity($rs)
    {
        return new Author(
            $rs["name"],
            $rs["lastname"],
            new \DateTime($rs["birthdate"]),
            $rs["id"],
        );
    }

    protected function entityBindValues($stmt, $entity)
    {
        $stmt->bindValue("name", $entity->getName());
        $stmt->bindValue("lastname", $entity->getLastName());
        $stmt->bindValue("birthdate", $entity->getBirthdate()->format("Y-m-d"));
    }

    protected function entityBindValuesWithId($stmt, $entity)
    {
        $this->entityBindValues($stmt, $entity);
        $stmt->setId($entity->getId());
    }
}