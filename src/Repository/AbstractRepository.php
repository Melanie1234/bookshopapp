<?php 

namespace App\Repository;

use Exception;
use PDO;

abstract class AbstractRepository {

    private PDO $connection;
    
    protected string $addSQL;
    protected string $getAllSQL;
    protected string $getByIdSQL;
    protected string $updateSQL;
    protected string $deleteSQL;

    public function __construct (string $addSQL, string $getAllSQL, string $getByIdSQL, string $updateSQL, string $deleteSQL){
        $this->addSQL = $addSQL;
        $this->getAllSQL = $getAllSQL;
        $this->getByIdSQL = $getByIdSQL;
        $this->updateSQL = $updateSQL;
        $this->deleteSQL = $deleteSQL;
    }

    protected abstract function sqlToEntity(ResultSet $rs);

    protected abstract function entityBindValues($stmt, $entity);

    protected abstract function entityBindValuesWithId($stmt, $entity);
    

    public function getAllSQL(){
        $list = [];
        try {
            $connection = Database::getConnection();
            $stmt = $connection->prepare($this->getAllSQL);
            $stmt->execute();
            $rs = $stmt->fetchAll();
            foreach ($rs as $row) {
                $entity = $this->sqlToEntity($row);
                $list[] = $entity;
            }
        } catch (Exception $e) {
            echo $e->getMessage(); 
        }
        return $list;
    }


    public function getByIdSQL(int $id){
        try {
            $connection = Database::getConnection();
            $stmt = $connection->prepare($this->getByIdSQL);
            $stmt->bindParam("id", $id, PDO::PARAM_INT);
            $stmt->execute();
            $rs = $stmt->fetch();
            if($rs) {
                $entity = $this->sqlToEntity($rs);
                return $entity;
            }
        } catch (Exception $e) {
            echo $e->getMessage();  
        }
        return null;
    }

    public function addSQL($entity){
            $connection = Database::getConnection();
            $stmt = $connection->prepare($this->addSQL);
            $this->entityBindValues($stmt,$entity);
            $stmt->execute();

            $rs = $connection->lastInsertId();
            $entity->setId($rs);

        return true;
    }
    
    public function deleteSQL($id){
        try {
            $connection = Database::getConnection();
            $stmt = $connection->prepare($this->deleteSQL);
            $stmt->bindValue("id", $id);
            return $stmt->execute();
        } catch (Exception $e) {
           echo $e->getMessage();
        }
    }

    public function updateSQL($entity){
            $connection = Database::getConnection();
            $stmt = $connection->prepare($this->updateSQL);
            $this->entityBindValuesWithId($stmt,$entity);
            return $stmt->execute();
    }
}