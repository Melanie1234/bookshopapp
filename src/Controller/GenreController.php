<?php 

namespace App\Controller;
use App\Entity\Genre;
use App\Repository\GenreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/genre')]
class GenreController extends AbstractController{
    private GenreRepository $genreRepository;
    private AbstractController $abstractController;

    public function __construct(GenreRepository $genreRepository)
    {
        $this->genreRepository = $genreRepository;
    }

    #[Route(methods:'GET')]
    public function all()
    {
    $genres = $this->genreRepository->getAllSQL();
    return $this->json($genres);
    }

    #[Route('/{id}', methods:'GET')]
    public function one($id)
    {
        $genre = $this->genreRepository->getByIdSQL($id);
        if (!$genre) {
            throw new NotFoundHttpException();

        }
        return $this->json($genre);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        try {
            $genre = $serializer->deserialize($request->getContent(), Genre::class, 'json');
            $this->genreRepository->addSQL($genre);
            return $this->json($genre, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }
    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id)
    {
        $genre = $this->genreRepository->getByIdSQL($id);
        if (!$genre) {
            throw new NotFoundHttpException();
        }
        $this->genreRepository->deleteSQL($id);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $genre = $this->genreRepository->getByIdSQL($id);
        if (!$genre) {
            throw new NotFoundHttpException();
        }
        try {
            $toUpdate = $serializer->deserialize($request->getContent(), Genre::class, 'json');
            $toUpdate->setId($id);
            $this->genreRepository->updateSQL($toUpdate);
            return $this->json($toUpdate);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

}