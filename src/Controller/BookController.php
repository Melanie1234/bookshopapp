<?php

namespace App\Controller;

use App\Entity\Book;
use App\Repository\BookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/book')]
class BookController extends AbstractController
{
    private BookRepository $bookRepository;

    public function __construct(BookRepository $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    #[Route(methods: 'GET')]
    public function allBooksInfos()
    {
        $books = $this->bookRepository->allBooksInfos();
        return $this->json($books);
    }

    #[Route('/{id}', methods: 'GET')]
    public function oneBookInfos($id)
    {
        $book = $this->bookRepository->oneBookInfos($id);
        if (!$book) {
            throw new NotFoundHttpException();
        }
        return $this->json($book);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        try {
            $book = $serializer->deserialize($request->getContent(), Book::class, 'json');
            if ($book->getStock() == 0) {
                $book->setStatus('Out of stock');
            } else {
                $book->setStatus('Available');
            }
            $this->bookRepository->postBookInfos($book);
            return $this->json($book, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id)
    {
        $book = $this->bookRepository->getByIdSQL($id);
        if (!$book) {
            throw new NotFoundHttpException();
        }
        $this->bookRepository->deleteSQL($id);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $book = $this->bookRepository->getByIdSQL($id);
        if (!$book) {
            throw new NotFoundHttpException();
        }
        try {
            $toUpdate = $serializer->deserialize($request->getContent(), Book::class, 'json');
            $toUpdate->setId($id);
            $this->bookRepository->updateSQL($toUpdate);
            return $this->json($toUpdate);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/mostSold', methods: 'GET')]
    public function getMostSold()
    {
        $books = $this->bookRepository->mostSoldBook();
        return $this->json($books);
    }

    #[Route('/findisbn/{isbn}', methods: 'GET')]
    public function findByIsbn(Request $request, string $isbn): JsonResponse
    {
        $book = $this->bookRepository->findOneByIsbn($isbn);

        if ($book) {
            return $this->json($book);
        } else {
            return $this->json(['error' => 'Book not found'], Response::HTTP_NOT_FOUND);
        }
    }
}