<?php 

namespace App\Controller;

use App\Business\BookBusiness;
use App\Entity\Order;
use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/order')]
class OrderController extends AbstractController{
    private OrderRepository $orderRepository;
    private BookBusiness $bookBusiness;

    public function __construct(OrderRepository $orderRepository, BookBusiness $bookBusiness)
    {
        $this->orderRepository = $orderRepository;
        $this->bookBusiness = $bookBusiness;
    }

    #[Route(methods:'GET')]
    public function all()
    {
    $orders = $this->orderRepository->allOrdersInfos();
    return $this->json($orders);
    }

    #[Route('/{id}', methods:'GET')]
    public function one($id)
    {
        $order = $this->orderRepository->oneOrderInfos($id);
        if (!$order) {
            throw new NotFoundHttpException();
        }
        return $this->json($order);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        try {
            $order = $serializer->deserialize($request->getContent(), Order::class, 'json');
            $this->orderRepository->postOrderInfos($order);
            return $this->json($order, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }
    
    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id)
    {
        $order = $this->orderRepository->getByIdSQL($id);
        if (!$order) {
            throw new NotFoundHttpException();
        }
        $this->orderRepository->deleteSQL($id);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $order = $this->orderRepository->getByIdSQL($id);
        if (!$order) {
            throw new NotFoundHttpException();
        }
        try {
            $toUpdate = $serializer->deserialize($request->getContent(), Order::class, 'json');
            $toUpdate->setId($id);
            $this->orderRepository->updateSQL($toUpdate);
            return $this->json($toUpdate);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/{idBook}/{quantity}', methods:'PUT')]
    public function validate(int $idBook, int $quantity)
    {
        $idUser = $this->getUser()->getId();
        $order = $this->bookBusiness->validateOrder($idBook, $quantity, $idUser);
        return $this->json($order);
    }
}