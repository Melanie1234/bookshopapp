<?php

namespace App\Controller;

use App\Entity\Author;
use App\Repository\AuthorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/author')]
class AuthorController extends AbstractController
{
    private AuthorRepository $authorRepository;

    public function __construct(AuthorRepository $authorRepository)
    {
        $this->authorRepository = $authorRepository;
    }

    #[Route(methods: 'GET')]
    public function all()
    {
        $authors = $this->authorRepository->getAllSQL();
        return $this->json($authors);
    }

    #[Route('/{id}', methods: 'GET')]
    public function one($id)
    {
        $author = $this->authorRepository->getByIdSQL($id);
        if (!$author) {
            throw new NotFoundHttpException();
        }
        return $this->json($author);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        try {
            $author = $serializer->deserialize($request->getContent(), Author::class, 'json');
            $this->authorRepository->addSQL($author);
            return $this->json($author, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }
    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id)
    {
        $author = $this->authorRepository->getByIdSQL($id);
        if (!$author) {
            throw new NotFoundHttpException();
        }
        $this->authorRepository->deleteSQL($id);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $author = $this->authorRepository->getByIdSQL($id);
        if (!$author) {
            throw new NotFoundHttpException();
        }
        try {
            $toUpdate = $serializer->deserialize($request->getContent(), Author::class, 'json');
            $toUpdate->setId($id);
            $this->authorRepository->updateSQL($toUpdate);
            return $this->json($toUpdate);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }
}