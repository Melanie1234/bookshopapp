<?php 

namespace App\Controller;

use App\Business\BookBusiness;
use App\Entity\Sale;
use App\Repository\BookRepository;
use App\Repository\SaleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/sale')]
class SaleController extends AbstractController{
    private SaleRepository $saleRepository;
    private BookBusiness $bookBusiness;
    private Sale $sale;

    public function __construct(SaleRepository $saleRepository, BookRepository $bookRepository, BookBusiness $bookBusiness)
    {
        $this->saleRepository = $saleRepository;
        $this->bookRepository = $bookRepository;
        $this->bookBusiness = $bookBusiness;
    }

    #[Route(methods:'GET')]
    public function all()
    {
    $sales = $this->saleRepository->allSalesInfos();
    return $this->json($sales);
    }

    #[Route('/{id}', methods:'GET')]
    public function one($id)
    {
        $sale = $this->saleRepository->oneSaleInfos($id);
        if (!$sale) {
            throw new NotFoundHttpException();
        }
        return $this->json($sale);
    }

    #[Route('/{idBook}/{quantity}/{saleDate}', methods:'PUT')]
    public function sell(int $idBook, int $quantity, string $saleDate)
    {
        $idUser = $this->getUser()->getId();
        $sale = $this->bookBusiness->sellBook($idBook, $quantity, $idUser, $saleDate);
        return $this->json($sale);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        try {
            $sale = $serializer->deserialize($request->getContent(), Sale::class, 'json');
            $this->saleRepository->postSaleInfos($sale);
            return $this->json($sale, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }
    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id)
    {
        $sale = $this->saleRepository->getByIdSQL($id);
        if (!$sale) {
            throw new NotFoundHttpException();
        }
        $this->saleRepository->deleteSQL($id);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $sale = $this->saleRepository->getByIdSQL($id);
        if (!$sale) {
            throw new NotFoundHttpException();
        }
        try {
            $toUpdate = $serializer->deserialize($request->getContent(), Sale::class, 'json');
            $toUpdate->setId($id);
            $this->saleRepository->updateSQL($toUpdate);
            return $this->json($toUpdate);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }
}