<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

#[Route('/api/user')]
class UserController extends AbstractController
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    #[Route(methods: 'GET')]
    public function all()
    {
        $users = $this->userRepository->getAllSQL();
        return $this->json($users);
    }

    #[Route('/current', methods: 'GET')]
    public function current()
    {
        $user = $this->getUser();
        return $this->json($user);
    }

    #[Route('/{id}', methods: 'GET')]
    public function one($id)
    {
        $user = $this->userRepository->getByIdSQL($id);
        if (!$user) {
            throw new NotFoundHttpException();
        }
        return $this->json($user);
    }

    #[Route(methods: 'POST')]
    public function add(
        UserRepository $repo,
        Request $request,
        SerializerInterface $serializer,
        UserPasswordHasherInterface $hasher,
        ValidatorInterface $validator
    ): JsonResponse {
        try {
            $user = $serializer->deserialize($request->getContent(), User::class, 'json');

        } catch (\Exception $e) {
            return $this->json('Invalid body', 400);
        }

        $errors = $validator->validate($user);
        if ($errors->count() > 0) {
            return $this->json(['errors' => $errors], 400);
        }

        if ($repo->findByEmail($user->getEmail())) {
            return $this->json('User Already exists', 400);
        }

        $hash = $hasher->hashPassword($user, $user->getPassword());
        $user->setPassword($hash);
        $user->setRole('ROLE_USER');
        $repo->addSQL($user);

        return $this->json($user, 201);
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $user = $this->userRepository->getByIdSQL($id);
        if (!$user) {
            throw new NotFoundHttpException();
        }
        try {
            $toUpdate = $serializer->deserialize($request->getContent(), User::class, 'json');
            $toUpdate->setId($id);
            $this->userRepository->updateSQL($toUpdate);
            return $this->json($toUpdate);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }
}