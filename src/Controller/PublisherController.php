<?php 

namespace App\Controller;
use App\Entity\Publisher;
use App\Repository\PublisherRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/publisher')]
class PublisherController extends AbstractController{
    private PublisherRepository $publisherRepository;
    private AbstractController $abstractController;

    public function __construct(PublisherRepository $publisherRepository)
    {
        $this->publisherRepository = $publisherRepository;
    }

    #[Route(methods:'GET')]
    public function all()
    {
    $publishers = $this->publisherRepository->getAllSQL();
    return $this->json($publishers);
    }

    #[Route('/{id}', methods:'GET')]
    public function one($id)
    {
        $publisher = $this->publisherRepository->getByIdSQL($id);
        if (!$publisher) {
            throw new NotFoundHttpException();

        }
        return $this->json($publisher);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        try {
            $publisher = $serializer->deserialize($request->getContent(), Publisher::class, 'json');
            $this->publisherRepository->addSQL($publisher);
            return $this->json($publisher, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }
    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id)
    {
        $publisher = $this->publisherRepository->getByIdSQL($id);
        if (!$publisher) {
            throw new NotFoundHttpException();
        }
        $this->publisherRepository->deleteSQL($id);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $author = $this->publisherRepository->getByIdSQL($id);
        if (!$author) {
            throw new NotFoundHttpException();
        }
        try {
            $toUpdate = $serializer->deserialize($request->getContent(), Publisher::class, 'json');
            $toUpdate->setId($id);
            $this->publisherRepository->updateSQL($toUpdate);
            return $this->json($toUpdate);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }
}