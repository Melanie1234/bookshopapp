<?php

namespace App\Entity;

use DateTime;
use Symfony\Component\Validator\Constraints as Assert;

class Book
{
	#[Assert\NotBlank]
	private ?int $id;
	#[Assert\NotBlank]
	private ?string $title;
	#[Assert\NotBlank]
	private ?string $isbn13;
	private ?DateTime $publicationDate;
	private ?string $comment;
	#[Assert\NotBlank]
	private ?int $stock;
	#[Assert\NotBlank]
	private ?string $status;
	#[Assert\NotBlank]
	private ?float $price;
	#[Assert\NotBlank]
	private ?int $idGenre;
	private ?int $idAuthor;
	#[Assert\NotBlank]
	private ?int $idPublisher;
	private ?int $idOrder;

	private Author $author;
	private Genre $genre;
	private Publisher $publisher;

	public function __construct(?string $title = null, ?string $isbn13 = null, ?DateTime $publicationDate = null, ?string $comment = null, ?int $stock = null, ?string $status = null, ?float $price = null, ?int $idGenre = null, ?int $idAuthor = null, ?int $idPublisher = null, ?int $id = null)
	{
		$this->title = $title;
		$this->isbn13 = $isbn13;
		$this->publicationDate = $publicationDate;
		$this->comment = $comment;
		$this->stock = $stock;
		$this->status = $status;
		$this->price = $price;
		$this->idGenre = $idGenre;
		$this->idAuthor = $idAuthor;
		$this->idPublisher = $idPublisher;
		$this->id = $id;
	}

	/**
	 * @return 
	 */
	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitle(): ?string
	{
		return $this->title;
	}

	/**
	 * @param string $title 
	 * @return self
	 */
	public function setTitle(?string $title): self
	{
		$this->title = $title;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getIsbn13(): ?string
	{
		return $this->isbn13;
	}

	/**
	 * @param string $isbn13 
	 * @return self
	 */
	public function setIsbn13(?string $isbn13): self
	{
		$this->isbn13 = $isbn13;
		return $this;
	}

	/**
	 * @return DateTime
	 */
	public function getPublicationDate(): ?DateTime
	{
		return $this->publicationDate;
	}

	/**
	 * @param DateTime $publishDate 
	 * @return self
	 */
	public function setPublishDate(?DateTime $publishDate): self
	{
		$this->publishDate = $publishDate;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getComment(): ?string
	{
		return $this->comment;
	}

	/**
	 * @param string $comment 
	 * @return self
	 */
	public function setComment(?string $comment): self
	{
		$this->comment = $comment;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getStock(): ?int
	{
		return $this->stock;
	}

	/**
	 * @param int $stock 
	 * @return self
	 */
	public function setStock(?int $stock): self
	{
		$this->stock = $stock;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getStatus(): ?string
	{
		return $this->status;
	}

	/**
	 * @param string $status 
	 * @return self
	 */
	public function setStatus(?string $status): self
	{
		$this->status = $status;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getPrice(): ?float
	{
		return $this->price;
	}

	/**
	 * @param float $price 
	 * @return self
	 */
	public function setPrice(?float $price): self
	{
		$this->price = $price;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getIdGenre(): ?int
	{
		return $this->idGenre;
	}

	/**
	 * @param int $idGenre 
	 * @return self
	 */
	public function setIdGenre(?int $idGenre): self
	{
		$this->idGenre = $idGenre;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getIdAuthor(): ?int
	{
		return $this->idAuthor;
	}

	/**
	 * @param int $idAuthor 
	 * @return self
	 */
	public function setIdAuthor(?int $idAuthor): self
	{
		$this->idAuthor = $idAuthor;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getIdPublisher(): ?int
	{
		return $this->idPublisher;
	}

	/**
	 * @param int $idPublisher 
	 * @return self
	 */
	public function setIdPublisher(?int $idPublisher): self
	{
		$this->idPublisher = $idPublisher;
		return $this;
	}

	/**
	 * @return Author
	 */
	public function getAuthor(): ?Author
	{
		return $this->author;
	}

	/**
	 * @param Author $author 
	 * @return self
	 */
	public function setAuthor(?Author $author): self
	{
		$this->author = $author;
		return $this;
	}

	/**
	 * @return Genre
	 */
	public function getGenre(): ?Genre
	{
		return $this->genre;
	}

	/**
	 * @param Genre $genre 
	 * @return self
	 */
	public function setGenre(?Genre $genre): self
	{
		$this->genre = $genre;
		return $this;
	}

	/**
	 * @return Publisher
	 */
	public function getPublisher(): ?Publisher
	{
		return $this->publisher;
	}

	/**
	 * @param Publisher $publisher 
	 * @return self
	 */
	public function setPublisher(?Publisher $publisher): self
	{
		$this->publisher = $publisher;
		return $this;
	}
}