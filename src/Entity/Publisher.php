<?php 

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Publisher {
    #[Assert\NotBlank]
    private ?int $id;
    #[Assert\NotBlank]
    private ?string $publisherLabel;

	public function __construct(string $publisherLabel = null, ?int $id = null) {
		$this->publisherLabel = $publisherLabel;
		$this->id = $id;
	}


	/**
	 * @return int
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getPublisherLabel(): ?string {
		return $this->publisherLabel;
	}
	
	/**
	 * @param string $publisherLabel 
	 * @return self
	 */
	public function setPublisherLabel(?string $publisherLabel): self {
		$this->publisherLabel = $publisherLabel;
		return $this;
	}
}