<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Genre {
    #[Assert\NotBlank]
    private ?int $id;
    #[Assert\NotBlank]
    private ?string $genreLabel;

	public function __construct(string $genreLabel = null, ?int $id = null) {
		$this->genreLabel = $genreLabel;
		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getGenreLabel(): ?string {
		return $this->genreLabel;
	}
	
	/**
	 * @param string $genreLabel 
	 * @return self
	 */
	public function setGenreLabel(?string $genreLabel): self {
		$this->genreLabel = $genreLabel;
		return $this;
	}
}