<?php 

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

class User implements UserInterface, PasswordAuthenticatedUserInterface {
    private ?int $id;
    #[Assert\NotBlank]
    private ?string $firstname;
    #[Assert\NotBlank]
    private ?string $lastname;
    #[Assert\NotBlank]
    private ?string $email;
    #[Assert\NotBlank]
    private ?string $bookshop;
    #[Assert\NotBlank]
    private ?string $password;
	private ?int $idAdmin;
	private ?string $role;



	public function __construct(?string $firstname = null, ?string $lastname = null, ?string $email = null,
    ?string $bookshop = null, ?string $password = null, ?int $idAdmin = null, ?string $role = null, ?int $id = null) {
    $this->firstname = $firstname;
    $this->lastname = $lastname;
    $this->email = $email;
    $this->bookshop = $bookshop;
    $this->password = $password;
    $this->idAdmin = $idAdmin;
	$this->role = $role; 
    $this->id = $id;
}

	/**
	 * @return int
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getFirstname(): ?string {
		return $this->firstname;
	}
	
	/**
	 * @param string $firstname 
	 * @return self
	 */
	public function setFirstname(?string $firstname): self {
		$this->firstname = $firstname;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getLastname(): ?string {
		return $this->lastname;
	}
	
	/**
	 * @param string $lastname 
	 * @return self
	 */
	public function setLastname(?string $lastname): self {
		$this->lastname = $lastname;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getEmail(): ?string {
		return $this->email;
	}
	
	/**
	 * @param string $email 
	 * @return self
	 */
	public function setEmail(?string $email): self {
		$this->email = $email;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getBookshop(): ?string {
		return $this->bookshop;
	}
	
	/**
	 * @param string $bookshop 
	 * @return self
	 */
	public function setBookshop(?string $bookshop): self {
		$this->bookshop = $bookshop;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getPassword(): ?string {
		return $this->password;
	}
	
	/**
	 * @param string $password 
	 * @return self
	 */
	public function setPassword(?string $password): self {
		$this->password = $password;
		return $this;
	}

	
	/**
	 * @return int
	 */
	public function getIdAdmin(): ?int {
		return $this->idAdmin;
	}
	
	/**
	 * @param int $idAdmin 
	 * @return self
	 */
	public function setIdAdmin(?int $idAdmin): self {
		$this->idAdmin = $idAdmin;
		return $this;
	}

    
    public function getRoles(): array {
        return [$this->role];
	}

	public function eraseCredentials() {
	}

	public function getUserIdentifier(): string {
        return $this->email;
	}

	/**
	 * @return string
	 */
	public function getRole(): ?string {
		return $this->role;
	}
	
	/**
	 * @param string $role 
	 * @return self
	 */
	public function setRole(?string $role): self {
		$this->role = $role;
		return $this;
	}
}