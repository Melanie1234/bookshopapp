<?php 

namespace App\Entity;

use DateTime;
use Symfony\Component\Validator\Constraints as Assert;

class Author {
    #[Assert\NotBlank]
    private ?int $id;
    #[Assert\NotBlank]
    private ?string $name;
    private ?string $lastname;
    private ?DateTime $birthdate;

	public function __construct(?string $name = null, ?string $lastname = null, ?DateTime $birthdate = null, ?int $id = null) {
		$this->name = $name;
		$this->lastname = $lastname;
		$this->birthdate = $birthdate;
		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @param string $name 
	 * @return self
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getLastname(): string {
		return $this->lastname;
	}
	
	/**
	 * @param string $lastname 
	 * @return self
	 */
	public function setLastname(string $lastname): self {
		$this->lastname = $lastname;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getBirthdate(): DateTime {
		return $this->birthdate;
	}
	
	/**
	 * @param DateTime $birthdate 
	 * @return self
	 */
	public function setBirthdate(DateTime $birthdate): self {
		$this->birthdate = $birthdate;
		return $this;
	}
}