<?php

namespace App\Entity;

use DateTime;
use Symfony\Component\Validator\Constraints as Assert;

class Order
{
    #[Assert\NotBlank]
    private ?int $id;
    #[Assert\NotBlank]
    private ?DateTime $receptionDate;
    #[Assert\NotBlank]
    private ?int $bookQuantity;
    #[Assert\NotBlank]
    private ?float $total;
    private ?string $comment;
    #[Assert\NotBlank]
    private ?int $idUser;
    #[Assert\NotBlank]
    private ?int $idBook;
    private ?User $user;
    private ?Book $book;

    public function __construct(?DateTime $receptionDate = null, ?int $bookQuantity = null, ?float $total = null, ?string $comment = null, ?int $idUser = null, ?int $idBook = null, ?int $id = null)
    {
        $this->receptionDate = $receptionDate;
        $this->bookQuantity = $bookQuantity;
        $this->total = $total;
        $this->comment = $comment;
        $this->idUser = $idUser;
        $this->idBook = $idBook;
        $this->id = $id;
    }
    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getReceptionDate(): ?DateTime
    {
        return $this->receptionDate;
    }

    /**
     * @param DateTime|null $receptionDate
     * @return self
     */
    public function setReceptionDate(?DateTime $receptionDate): self
    {
        $this->receptionDate = $receptionDate;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getBookQuantity(): ?int
    {
        return $this->bookQuantity;
    }

    /**
     * @param int|null $bookQuantity
     * @return self
     */
    public function setBookQuantity(?int $bookQuantity): self
    {
        $this->bookQuantity = $bookQuantity;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getTotal(): ?float
    {
        return $this->total;
    }

    /**
     * @param float|null $total
     * @return self
     */
    public function setTotal(?float $total): self
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     * @return self
     */
    public function setComment(?string $comment): self
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getIdUser(): ?int
    {
        return $this->idUser;
    }

    /**
     * @param int|null $idUser
     * @return self
     */
    public function setIdUser(?int $idUser): self
    {
        $this->idUser = $idUser;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getIdBook(): ?int
    {
        return $this->idBook;
    }

    /**
     * @param int|null $idBook
     * @return self
     */
    public function setIdBook(?int $idBook): self
    {
        $this->idBook = $idBook;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user 
     * @return self
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Book
     */
    public function getBook(): ?Book
    {
        return $this->book;
    }

    /**
     * @param Book $book 
     * @return self
     */
    public function setBook(?Book $book): self
    {
        $this->book = $book;
        return $this;
    }
}