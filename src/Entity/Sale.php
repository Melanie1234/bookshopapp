<?php

namespace App\Entity;

use DateTime;
use Symfony\Component\Validator\Constraints as Assert;

class Sale
{
	#[Assert\NotBlank]
	private ?int $id;
	#[Assert\NotBlank]
	private ?DateTime $saleDate;
	#[Assert\NotNull]
	private ?int $bookQuantity;
	#[Assert\NotBlank]
	private ?int $idUser;
	#[Assert\NotBlank]
	private ?int $idBook;
	private ?User $user;
	private ?Book $book;

	public function __construct(?DateTime $saleDate = null, ?int $bookQuantity = null, ?int $idUser = null, ?int $idBook = null, ?int $id = null)
	{
		$this->saleDate = $saleDate;
		$this->bookQuantity = $bookQuantity;
		$this->idUser = $idUser;
		$this->idBook = $idBook;
		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(?int $id): self
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return DateTime
	 */
	public function getSaleDate(): ?DateTime
	{
		return $this->saleDate;
	}

	/**
	 * @param DateTime $saleDate 
	 * @return self
	 */
	public function setSaleDate(?DateTime $saleDate): self
	{
		$this->saleDate = $saleDate;
		return $this;
	}

	public function getBookQuantity(): ?int
	{
		return $this->bookQuantity;
	}

	/**
	 * @param int $bookQuantity 
	 * @return self
	 */
	public function setBookQuantity(?int $bookQuantity): self
	{
		$this->bookQuantity = $bookQuantity;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getIdUser(): ?int
	{
		return $this->idUser;
	}

	/**
	 * @param int $idUser 
	 * @return self
	 */
	public function setIdUser(?int $idUser): self
	{
		$this->idUser = $idUser;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getIdBook(): ?int
	{
		return $this->idBook;
	}

	/**
	 * @param int $idBook 
	 * @return self
	 */
	public function setIdBook(?int $idBook): self
	{
		$this->idBook = $idBook;
		return $this;
	}

	/**
	 * @return User
	 */
	public function getUser(): ?User
	{
		return $this->user;
	}

	/**
	 * @param User $user 
	 * @return self
	 */
	public function setUser(?User $user): self
	{
		$this->user = $user;
		return $this;
	}

	/**
	 * @return Book
	 */
	public function getBook(): ?Book
	{
		return $this->book;
	}

	/**
	 * @param Book $book 
	 * @return self
	 */
	public function setBook(?Book $book): self
	{
		$this->book = $book;
		return $this;
	}
}