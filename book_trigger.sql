-- Active: 1673947741321@@127.0.0.1@3306@bookshop
-- verifie le stock du book et si un order est déjà en cours de route.
-- Dans le cas où le stock est à zéro et aucune commande n'est déjà passée, il insère un nouvel order.
DROP TRIGGER IF EXISTS check_stock;

CREATE TRIGGER CHECK_STOCK AFTER UPDATE ON BOOK FOR 
EACH ROW BEGIN DECLARE 
	DECLARE existing_order_count INT;
	SELECT
	    COUNT(*) INTO existing_order_count
	FROM `order`
	WHERE id_book = NEW.id;
	IF NEW.stock = 0
	AND existing_order_count = 0 THEN
	INSERT INTO
	    `order` (
	        order_date,
	        reception_date,
	        book_quantity,
	        total,
	        comment,
	        id_user,
	        id_book
	    )
	VALUES (
	        NOW(),
	        null,
	        10,
	        10 * NEW.price,
	        'automatique',
	        null,
	        NEW.id
	    );
	END IF;
	END;
