-- Active: 1674037180654@@127.0.0.1@3306@bookshop

DROP DATABASE bookshopapp;

CREATE DATABASE bookshopapp;

USE bookshopapp;

CREATE TABLE
    genre(
        id INT PRIMARY KEY AUTO_INCREMENT,
        genre_label VARCHAR(255)
    );

CREATE TABLE
    publisher(
        id INT PRIMARY KEY AUTO_INCREMENT,
        publisher_label VARCHAR(255)
    );

CREATE TABLE
    author(
        id INT PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(255) NOT NULL,
        lastname VARCHAR(255),
        birthdate DATE
    );

CREATE TABLE
    `admin`(
        id INT PRIMARY KEY AUTO_INCREMENT,
        firstname VARCHAR(255),
        lastname VARCHAR(255),
        email VARCHAR(255),
        bookshop VARCHAR(255),
        `password` VARCHAR(255)
    );

CREATE TABLE
    `user`(
        id INT PRIMARY KEY AUTO_INCREMENT,
        firstname VARCHAR(255),
        lastname VARCHAR(255),
        email VARCHAR(255),
        bookshop VARCHAR(255),
        `password` VARCHAR(255),
        id_admin INT,
        `role` VARCHAR(255),
        FOREIGN KEY (id_admin) REFERENCES `admin`(id) ON DELETE SET NULL
    );


CREATE TABLE
    book(
        id INT PRIMARY KEY AUTO_INCREMENT,
        title VARCHAR(255) NOT NULL,
        isbn_13 VARCHAR(13) NOT NULL,
        publication_date DATE NOT NULL,
        `comment` TEXT,
        stock INT NOT NULL,
        `status` VARCHAR(255) NOT NULL,
        price float NOT NULL,
        id_genre INT,
        id_author INT,
        id_publisher INT,
        FOREIGN KEY (id_genre) REFERENCES genre(id) ON DELETE SET NULL,
        FOREIGN KEY (id_author) REFERENCES author(id) ON DELETE SET NULL,
        FOREIGN KEY (id_publisher) REFERENCES publisher(id) ON DELETE SET NULL
    );

CREATE TABLE
    sale (
        id INT PRIMARY KEY AUTO_INCREMENT,
        sale_date DATE,
        book_quantity INT,
        id_user INT,
        id_book INT,
        FOREIGN KEY (id_user) REFERENCES `user`(id) ON DELETE SET NULL, 
        FOREIGN KEY (id_book) REFERENCES book(id) ON DELETE SET NULL
    );

CREATE TABLE
    `order` (
        id INT PRIMARY KEY AUTO_INCREMENT,
        order_date DATE,
        reception_date DATE,
        book_quantity INT,
        total FLOAT,
        `comment` TEXT,
        id_user INT,
        id_book INT,
        FOREIGN KEY (id_user) REFERENCES `user`(id) ON DELETE SET NULL,
        FOREIGN KEY (id_book) REFERENCES book(id) ON DELETE SET NULL
    );