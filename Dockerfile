# Utilisez une image de base PHP
FROM php:8.2-fpm

# Installez les dépendances requises
RUN apt-get update && apt-get install -y \
    unzip \
    git \
    && rm -rf /var/lib/apt/lists/*

# Installez Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Définir le répertoire de travail
WORKDIR /var/www

# Copier les fichiers de l'application
COPY . .

# Installer les dépendances PHP
RUN composer install

# Exposer le port pour le serveur
EXPOSE 8000

# Commande par défaut pour exécuter PHP-FPM
CMD ["php-fpm"]