-- Active: 1674037180654@@127.0.0.1@3306@bookshop

USE bookshopapp;
INSERT INTO genre (genre_label) VALUES
    ('polar'),
    ('roman'),
    ('autobiographie');

INSERT INTO publisher (publisher_label) VALUES
    ('Libretto'),
    ('Points'),
    ('Livre de Poche'),
    ('10/18'),
    ('Independently published'),
    ('Archipoche'),
    ('Folio Classique'),
    ('Audimat Editions');

INSERT INTO author (name, lastname, birthdate) VALUES
    ('Wilkie', 'Collins', '1824-01-08'),
    ('Elizabeth', 'Gaskell', '1810-09-29'),
    ('Liane', 'Moriarty', '1966-11-15'),
    ('Ian', 'Manook', '1949-08-13'),
    ('Charlotte', 'Brontë', '1816-04-21'),
    ('Louisa May', 'Alcott', '1832-11-29'),
    ('Lucy Maud', 'Montgomery', '1874-11-30'),
    ('Charles', 'Dickens', '1812-02-07'),
    ('Cosey Fanni', 'Tutti', NULL),
    ('Valerie', 'Perrin', '1967-01-19');

INSERT INTO `admin` (firstname, lastname, email, bookshop, `password`) VALUES
    ('AdminFirstName1', 'AdminLastName1', 'admin1@email.com', 'Bookstore A', 'adminpass1'),
    ('AdminFirstName2', 'AdminLastName2', 'admin2@email.com', 'Bookstore B', 'adminpass2');

INSERT INTO `user` (firstname, lastname, email, bookshop, `password`, id_admin) VALUES
    ('John', 'Doe', 'john.doe@email.com', 'Bookstore A', 'password123', 1),
    ('Jane', 'Smith', 'jane.smith@email.com', 'Bookstore B', 'securepass456', 2),
    ('Robert', 'Johnson', 'robert.johnson@email.com', 'Bookstore C', 'safe123pass', 1);
    
INSERT INTO book (title, isbn_13, publication_date, `comment`, stock, `status`, price, id_genre, id_author, id_publisher) VALUES
    ('Basil', '9782752908148', '1852-01-01', NULL, 2, 'Available', 10.80, 1, 1, 1), 
    ('Nord et Sud', '9782757820902', '1855-01-01', 2, 10, 'Available', 10.40, 2, 2, 2),
    ('À la recherche d''Alice Love', '9782253101673', '2009-01-01', NULL, 1, 'Available', 8.70, 2, 3, 3),
    ('Yeruldelgger', '9782253163886', '2013-01-01', NULL, 2, 'Available', 8.90, 2, 4, 3),
    ('Jane Eyre', '9791035816735', '1847-01-01', NULL, 1, 'Available', 8.90, 2, 5, 4),
    ('Les quatre filles du Docteur', '9798356191213', '1868-01-01', NULL, 2, 'Available', 10.99, 2, 6, 4),
    ('Anne et la maison aux pignons verts', '9782377359585', '1908-01-01', NULL, 2, 'Available', 8.95, 2, 7, 5),
    ('Contes de Noël', '9782070448845', '1882-01-01', NULL, 3, 'Available', 9.70, 1, 8, 5),
    ('Art Sexe Musique', '9782492469022', '2021-01-01', NULL, 2, 'Available', 20.00, 3, 9, 6),
    ('Changer L''eau Des Fleurs', '9782253238027', '2018-01-01', NULL, 1, 'Available', 9.90, 2, 10, 6);

INSERT INTO sale (sale_date, book_quantity, id_user, id_book) VALUES
    ('2023-01-15', 1, 3, 3),
    ('2023-02-20', 4, 2, 7),
    ('2023-03-10', 1, 1, 8);

INSERT INTO `order` (order_date, reception_date, book_quantity, total, `comment`, id_user, id_book) VALUES
    ('2023-01-10', '2023-01-20', 5, 75.00, 'Standard shipping', 1, 1),
    ('2023-02-05', '2023-02-15', 3, 45.00, 'Express shipping', 3, 2),
    ('2023-03-01', '2023-03-12', 7, 105.00, 'Standard shipping', 2, 3);